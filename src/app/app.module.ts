import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule} from "@angular/forms";
import {RouterModule, Routes} from "@angular/router";

import { AppComponent } from './app.component';
import { CarComponent } from './components/car/car.component';
import { ContactComponent } from './components/contact/contact.component';
import { TestComponent } from './components/test/test.component';
import {HttpClientModule} from "@angular/common/http";

const appRoutes: Routes = [
  {path: '', component: CarComponent},
  {path: 'about', component: ContactComponent},
  {path: 'test', component: TestComponent},
];

@NgModule({
  declarations: [
    AppComponent,
    CarComponent,
    ContactComponent,
    TestComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    BrowserModule,
    HttpClientModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
