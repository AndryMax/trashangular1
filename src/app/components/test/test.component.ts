import {Component, Injectable, OnInit, ElementRef, ViewChild} from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";

@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styleUrls: ['./test.component.css']
})

@Injectable()
export class TestComponent implements OnInit {
  @ViewChild('myCheck') check: ElementRef;

  customer: Customer = new Customer();
  customers: any;
  
  isNew: boolean;

  path: string;

  model: any = {
    id: '',
    firstName: '',
    lastName: '',
    address: '',
    budget: ''
  };

  receiverCustomer: Customer;
  done: boolean;

  constructor(private http: HttpClient) {
    this.path = "http://localhost:8083/api/v1/customers/";
    this.isNew = true;
  }

  ngOnInit() {
     this.http.get(this.path)
       .subscribe(data => this.customers = data);
  }

  onSubmit() {
    this.customer = {
      id: this.model.id,
      firstName: this.model.firstName,
      lastName: this.model.lastName,
      address: this.model.address,
      budget: this.model.budget,
    };
    // this.http.post("http://localhost:8083/api/v1/customers/",
    //   this.customer, {
    //   headers: up HttpHeaders().set('Content-type', 'application/json')
    //   }).subscribe((data: Customer) => {
    //     this.receiverCustomer = data; this.done = true;
    console.log(this.model.firstName);

    // }, error => console.log(error));
    this.http.post(this.path, this.customer, {
      headers: new HttpHeaders().set('Content-type', 'application/json')
    }).subscribe(customer => {
      if (this.isNew) {
        this.customers.push(customer)
      }
    });
    console.log(this.model.lastName);
    console.log(this.model.address);
    console.log(this.model.budget);
    console.log(this.model);
  }

  select(index: number){
    this.model = this.customers[index];
    this.isNew = false;
  }

  deleteCustomer(id: number, index: number){
    console.log(id);
    this.http.delete(this.path + id, {
      headers: new HttpHeaders().set('Content-type', 'application/json')
    }).subscribe(customerId => this.customers.splice(index, 1));
  }

  clear(){
    this.model = new Customer();
    this.isNew = true;
  }
}

export class Customer {

  id: number;
  firstName: string;
  lastName: string;
  address: string;
  budget: number;
}
