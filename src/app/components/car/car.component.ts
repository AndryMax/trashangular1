import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-car',
  templateUrl: './car.component.html',
  styleUrls: ['./car.component.css']
})
export class CarComponent implements OnInit {

  private name: string;
  private speed: number;
  private model: string;
  private colors: Colors;
  private options: string[];
  private isEdit: boolean;

  constructor() {
  }

  ngOnInit() {
    this.name = 'kek';
    this.speed = 234;
    this.model = 'rs8';
    this.colors = {
      car: 'красный',
      salon: 'black',
      wheels: 'silver'
    };
    this.options = ['a', 'b'];
  }

  public carSelect(carName: string) {
    switch (carName) {
      case 'bmw' : {
        this.name = 'BMW';
        this.speed = 300;
        this.model = 'i8';
        this.colors = {
          car: 'yellow',
          salon: 'white',
          wheels: 'kik'
        };
        this.options = ['c', 'd', 'w'];
        break;
      }
      case 'audi': {
        this.name = 'AUDI';
        this.speed = 200;
        this.model = 'l20';
        this.colors = {
          car: 'green',
          salon: 'gray',
          wheels: 'black'
        };
        this.options = ['tvoya', 'mamka'];
        break;
      }
      case 'bugatti': {
        this.name = 'Bugatti';
        this.speed = 350;
        this.model = 'r300';
        this.colors = {
          car: 'green',
          salon: 'gray',
          wheels: 'black'
        };
        this.options = ['tvoya', 'mamka'];
        break;
      }
    }
  }

  public addOpt(option:string) {
    this.options.unshift(option);
    return false;
  }

  public showEdit() {
    this.isEdit = !this.isEdit;
  }
}

interface Colors {
  car: string;
  salon:string;
  wheels:string;
}
